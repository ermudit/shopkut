package utility;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class SingleVolley {
    private static SingleVolley mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;

    private SingleVolley(Context context) {
        mCtx = context;
        requestQueue = getRequestQueue();
    }


    private RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        return requestQueue;
    }

    public static synchronized SingleVolley getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SingleVolley(context);
        }
        return mInstance;
    }

    public <T> void addToRequestQue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
