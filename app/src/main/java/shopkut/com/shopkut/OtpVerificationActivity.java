package shopkut.com.shopkut;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class OtpVerificationActivity extends AppCompatActivity {

    EditText ed_OTP;
    Button bt_OTP_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        initViews();

    }


    private void initViews() {
        ed_OTP = (EditText) findViewById(R.id.editTextotp);


        bt_OTP_send = (Button) findViewById(R.id.resetPasswordbtn);
        bt_OTP_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //isValidate();
                startActivity(new Intent(OtpVerificationActivity.this, ResetPassword.class));
                finish();
            }
        });


    }


    public boolean isValidate() {
        if (TextUtils.isEmpty(ed_OTP.getText())) {
            ed_OTP.setError("Please Enter OTP");
            ed_OTP.requestFocus();
            return false;
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                startActivity(new Intent(OtpVerificationActivity.this, ForgetPassword.class));
                this.finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(OtpVerificationActivity.this, ForgetPassword.class));
        // your code.
    }

}