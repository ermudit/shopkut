package shopkut.com.shopkut;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import constants.Constants;
import utility.SingleVolley;

public class SignUpActivity extends AppCompatActivity {

    TextView tv_SIGNUPA_referal, tv_SIGNUPA_by_sign, tv_SIGNUPA_member;
    EditText ed_SIGNUPA_name, ed_SIGNUPA_email, ed_SIGNUPA_password, ed_SIGNUPA_mobile;
    RadioButton rd_SIGNUPA_male, rd_SIGNUPA_selected_radio_btn, rd_SIGNUPA_female;
    RadioGroup rg_SIGNUPA;
    Button bt_SIGNUPA_submit;
    static String name, password, emailAddress, mobileNumber;
    String text, sign, member;
    public String rd_value;
    String selectedtext = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        initViews();

    }


    private void initViews() {

        rd_SIGNUPA_male = (RadioButton) findViewById(R.id.radio_male);
        rd_SIGNUPA_female = (RadioButton) findViewById(R.id.radio_female);
        rg_SIGNUPA = (RadioGroup) findViewById(R.id.radio_gender);
        ed_SIGNUPA_name = (EditText) findViewById(R.id.editTextname);
        ed_SIGNUPA_email = (EditText) findViewById(R.id.editTextemail);
        ed_SIGNUPA_password = (EditText) findViewById(R.id.editTextpassword);
        ed_SIGNUPA_mobile = (EditText) findViewById(R.id.editTextMobile);
        ed_SIGNUPA_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        ed_SIGNUPA_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ed_SIGNUPA_password.setSelection(ed_SIGNUPA_password.length());
        bt_SIGNUPA_submit = (Button) findViewById(R.id.loginbtn);
        tv_SIGNUPA_referal = (TextView) findViewById(R.id.tv_later);
        text = "Enter Referrel Code and Earn <font color='grey'></font><font color='red'>Click Here</font>.";
        tv_SIGNUPA_referal.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        tv_SIGNUPA_by_sign = (TextView) findViewById(R.id.tv_by_sign);
        sign = "By signing up you will agree <font color='grey'></font><font color='red'>Privacy Policy</font><font color='red'> and</font><font color='red'> Terms Of Service";
        tv_SIGNUPA_by_sign.setText(Html.fromHtml(sign), TextView.BufferType.SPANNABLE);
        tv_SIGNUPA_member = (TextView) findViewById(R.id.tv_already_member);
        member = "Already a member? <font color='grey'></font><font color='red'>LOGIN</font>.";
        tv_SIGNUPA_member.setText(Html.fromHtml(member), TextView.BufferType.SPANNABLE);
        name = ed_SIGNUPA_name.getText().toString().trim();
        password = ed_SIGNUPA_password.getText().toString().trim();
        emailAddress = ed_SIGNUPA_email.getText().toString().trim();
        mobileNumber = ed_SIGNUPA_mobile.getText().toString().trim();

        tv_SIGNUPA_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);

            }
        });

        rg_SIGNUPA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup,
                                         int radioButtonID) {

                if (rd_SIGNUPA_male.isChecked()) {
                    rd_value = rd_SIGNUPA_male.getText().toString();
                    rd_SIGNUPA_selected_radio_btn = (RadioButton) rg_SIGNUPA.getChildAt(0);
                    selectedtext = rd_SIGNUPA_selected_radio_btn.getText().toString();

                } else if (rd_SIGNUPA_female.isChecked()) {
                    rd_value = rd_SIGNUPA_female.getText().toString();
                    rd_SIGNUPA_selected_radio_btn = (RadioButton) rg_SIGNUPA.getChildAt(1);
                    selectedtext = rd_SIGNUPA_selected_radio_btn.getText().toString();

                }
            }
        });


        bt_SIGNUPA_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isValidate();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.SIGNUP,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                                    String status = jsonObject1.getString("status");
                                    String success_msg = jsonObject1.getString("successmsg").trim();
                                    String check = "0";
                                    //  {"user":{"status":"0","type":"android","error":"201","errormsg":"User already exist"}}

                                    if (success_msg.equals("User already registered") || success_msg.equals("User successfully registered"))

                                    {
                                        Toast.makeText(SignUpActivity.this, "" + success_msg, Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                                        startActivity(i);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", ed_SIGNUPA_email.getText().toString().trim());
                        params.put("name", ed_SIGNUPA_name.getText().toString().trim());
                        params.put("password", ed_SIGNUPA_password.getText().toString().trim());
                        params.put("mobile", ed_SIGNUPA_mobile.getText().toString().trim());
                        params.put("gender", selectedtext);
                        return params;
                    }

                };

                SingleVolley.getmInstance(SignUpActivity.this).addToRequestQue(stringRequest);
            }
        });
    }

    public boolean isValidate() {
        if (TextUtils.isEmpty(ed_SIGNUPA_name.getText().toString())) {
            ed_SIGNUPA_name.requestFocus();
            ed_SIGNUPA_name.setError("Please Enter Name");
            return false;
        }
        if (!isValidEmail(ed_SIGNUPA_email.getText().toString())) {
            ed_SIGNUPA_email.requestFocus();
            ed_SIGNUPA_email.setError("Please Enter Email");
            return false;
        }

        if (!isValidMobile(ed_SIGNUPA_mobile.getText().toString())) {

            ed_SIGNUPA_mobile.requestFocus();
            ed_SIGNUPA_mobile.setError("Please Enter Mobile Number");
            return false;
        } else if (!isValidPassword(ed_SIGNUPA_password.getText().toString())) {
            ed_SIGNUPA_password.requestFocus();
            ed_SIGNUPA_password.setError("Please Enter Password");
            return false;
        } else {
            ed_SIGNUPA_email.setError(null);
            ed_SIGNUPA_password.setError(null);
        }
        if (rd_SIGNUPA_male.isChecked() || rd_SIGNUPA_female.isChecked()) {

        } else {
            Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_LONG).show();

        }

        return true;
    }

    private boolean isValidEmail(String emailAddress) {
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    private boolean isValidMobile(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                this.finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));

    }
}