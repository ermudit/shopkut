package shopkut.com.shopkut;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class ResetPassword extends AppCompatActivity {

    EditText ed_RESET_PASSWORD_otp, ed_RESET_PASSWORD_new_password, ed_RESET_PASSWORD_confirm_password;
    Button bt_RESET_PASSWORD_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        initViews();
        isValidate();

    }


    private void initViews() {
        ed_RESET_PASSWORD_otp = (EditText) findViewById(R.id.editTextotp);
        ed_RESET_PASSWORD_new_password = (EditText) findViewById(R.id.editTextnewpassword);
        ed_RESET_PASSWORD_confirm_password = (EditText) findViewById(R.id.editTextconfirmpassword);


        bt_RESET_PASSWORD_send = (Button) findViewById(R.id.resetPasswordbtn);
        bt_RESET_PASSWORD_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidate();
                startActivity(new Intent(ResetPassword.this, LoginActivity.class));
                finish();
            }
        });


    }


    public boolean isValidate() {
        if (TextUtils.isEmpty(ed_RESET_PASSWORD_otp.getText())) {
            ed_RESET_PASSWORD_otp.requestFocus();
            ed_RESET_PASSWORD_otp.setError("Please Enter OTP");

            return false;
        }
        if (TextUtils.isEmpty(ed_RESET_PASSWORD_new_password.getText())) {
            ed_RESET_PASSWORD_new_password.requestFocus();
            ed_RESET_PASSWORD_new_password.setError("Please Enter New Password");

            return false;
        }
        if (TextUtils.isEmpty(ed_RESET_PASSWORD_confirm_password.getText())) {
            ed_RESET_PASSWORD_confirm_password.requestFocus();
            ed_RESET_PASSWORD_confirm_password.setError("Please Re-Enter New Password");
            return false;
        }
        else {
            ed_RESET_PASSWORD_otp.setError(null);
            ed_RESET_PASSWORD_new_password.setError(null);
            ed_RESET_PASSWORD_confirm_password.setError(null);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                startActivity(new Intent(ResetPassword.this, OtpVerificationActivity.class));
                this.finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ResetPassword.this, OtpVerificationActivity.class));
        // your code.
    }

}