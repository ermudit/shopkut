package shopkut.com.shopkut;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import constants.Constants;
import preference.SharedPref;
import utility.SingleVolley;


public class MyProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {


    private final int IMG_REQUEST = 1;
    private final int GALLERY = 2;
    EditText ed_MY_PROFILEA_email, ed_MY_PROFILEA_mobile, ed_MY_PROFILEA_date, ed_MY_PROFILEA_addrress, ed_MY_PROFILEA_password, ed_MY_PROFILEA_name;
    TextView tv_MY_PROFILEA_logout, tv_MY_PROFILEA_admin;
    String email, mobile, date, address, password;
    Button bt_MY_PROFILEA_submit;
    private int mYear, mMonth, mDay;
    static final int DATE_DIALOG_ID = 0;
    private String TAG = MyProfileActivity.class.getSimpleName();
    ImageView img_MY_PROFILEA, img_MY_PROFILEA_camera_open, img_MY_PROFILEA_gallery_open;
    SharedPref mPreferences;
    private Uri mCropImageUri;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener datepic;
    String restore = "";
    Bitmap bitmap;
    private Uri picUri;
    private String selectedImagePath;


    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        datepic = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        initViews();

        //{"user":{"status":"1","success":"200","successmsg":"User record","user_id":"2","name":"Kittu Avasthi","email":"kittuavasthi@yahoo.in","gender":"","profile_pic":"https:\/\/graph.facebook.com\/1921130828104782\/picture?type=large"}}
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GETLOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                            ed_MY_PROFILEA_email.setText(jsonObject1.getString("email"));
                            ed_MY_PROFILEA_name.setText(jsonObject1.getString("name"));
                            ed_MY_PROFILEA_mobile.setText(jsonObject1.getString("mobile"));
                            if (!(jsonObject1.getString("address").equals("null"))) {
                                ed_MY_PROFILEA_addrress.setText(jsonObject1.getString("address"));
                                ed_MY_PROFILEA_date.setText(jsonObject1.getString("dob"));
                            } else {
                                ed_MY_PROFILEA_addrress.setText("");
                                ed_MY_PROFILEA_date.setText("");
                            }
                            //  ed_MY_PROFILEA_password.setText(jsonObject1.getString("password"));


                            String profile_pic = jsonObject1.getString("profile_pic");

                            Glide.with(MyProfileActivity.this).load(profile_pic).placeholder(R.mipmap.profile_pic).dontAnimate().into(img_MY_PROFILEA);
                            if(mPreferences.getOrigin().equals("null origin"))
                            {
                                img_MY_PROFILEA_camera_open.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPreferences.getUserId());
                params.put("origin", mPreferences.getOrigin());

                return params;
            }


        };

        SingleVolley.getmInstance(MyProfileActivity.this).addToRequestQue(stringRequest);
    }

    private void initViews() {
        ed_MY_PROFILEA_email = (EditText) findViewById(R.id.editTextemail);
        mPreferences = SharedPref.getInstance(MyProfileActivity.this);
        img_MY_PROFILEA = (ImageView) findViewById(R.id.img_profile);
        ed_MY_PROFILEA_mobile = (EditText) findViewById(R.id.editTextMobile);
        ed_MY_PROFILEA_password = (EditText) findViewById(R.id.editTextpassword);
        ed_MY_PROFILEA_date = (EditText) findViewById(R.id.editTextdate);
        ed_MY_PROFILEA_addrress = (EditText) findViewById(R.id.editTextaddress);
        bt_MY_PROFILEA_submit = (Button) findViewById(R.id.bt_submit);
        ed_MY_PROFILEA_name = (EditText) findViewById(R.id.editTextname);
        tv_MY_PROFILEA_logout = (TextView) findViewById(R.id.tv_sign_out);
        img_MY_PROFILEA_camera_open = (ImageView) findViewById(R.id.camera_open);



        tv_MY_PROFILEA_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreferences.clearPreferences();
                LoginManager.getInstance().logOut();
                LoginActivity.i = 0;
                LoginActivity.j = 0;
                mPreferences.setIsLogin(false);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGOUT,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(MyProfileActivity.this, "" + response, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(MyProfileActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


                SingleVolley.getmInstance(MyProfileActivity.this).addToRequestQue(stringRequest);
            }
        });


        bt_MY_PROFILEA_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isValidate();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.USER_EDIT_PROFILE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    Toast.makeText(MyProfileActivity.this, "" + response, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("user_records");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", mPreferences.getUserId());
                        params.put("email", ed_MY_PROFILEA_email.getText().toString());
                        params.put("password", ed_MY_PROFILEA_password.getText().toString());
                        params.put("gender", "");

                        params.put("user_image", imageToString(bitmap));
                        params.put("origin", mPreferences.getOrigin());
                        params.put("dob", ed_MY_PROFILEA_date.getText().toString());
                        params.put("address", ed_MY_PROFILEA_addrress.getText().toString());
                        params.put("mobile", ed_MY_PROFILEA_mobile.getText().toString());
                        return params;
                    }

                };

                SingleVolley.getmInstance(MyProfileActivity.this).addToRequestQue(stringRequest);
            }
        });

        ed_MY_PROFILEA_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(MyProfileActivity.this, datepic, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        img_MY_PROFILEA_camera_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectImageClick(v);
            }
        });



        email = ed_MY_PROFILEA_email.getText().toString().trim();
        password = ed_MY_PROFILEA_password.getText().toString().trim();
        mobile = ed_MY_PROFILEA_mobile.getText().toString().trim();
        date = ed_MY_PROFILEA_date.getText().toString().trim();
        address = ed_MY_PROFILEA_addrress.getText().toString().trim();

    }

    private void selectImage() {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(i, IMG_REQUEST);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        ed_MY_PROFILEA_date.setText(sdf.format(myCalendar.getTime()));
    }


    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);

        }

        return null;

    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            ed_MY_PROFILEA_date.setText(new StringBuilder().append(mDay).append("/").append(mMonth + 1).append("/").append(mYear));

        }

    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.refresh, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(MyProfileActivity.this, MainActivity.class));
                return true;
            case R.id.refresh:
                Intent intent=new Intent(MyProfileActivity.this,MyProfileActivity.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(MyProfileActivity.this, MainActivity.class));
        // your code.
    }

    public boolean isValidate() {

        if (!isValidEmail(ed_MY_PROFILEA_email.getText().toString())) {
            ed_MY_PROFILEA_email.requestFocus();
            ed_MY_PROFILEA_email.setError("Please Enter Email");
            return false;
        }
        if (ed_MY_PROFILEA_mobile.length() != 10) {
            ed_MY_PROFILEA_mobile.requestFocus();
            ed_MY_PROFILEA_mobile.setError("Please Enter Valid Mobile Number");
            return false;
        } else if (!isValidPassword(ed_MY_PROFILEA_password.getText().toString())) {
            ed_MY_PROFILEA_password.requestFocus();
            ed_MY_PROFILEA_password.setError("Please Enter Password");
            return false;
        } else {
            ed_MY_PROFILEA_email.setError(null);
            ed_MY_PROFILEA_password.setError(null);
        }


        return true;
    }


    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }

    private boolean isValidEmail(String emailAddress) {
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), result.getUri());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                img_MY_PROFILEA.setImageBitmap(bitmap);
                Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }


    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        Log.e("image", Base64.encodeToString(imgBytes, Base64.DEFAULT));
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);

    }

    @Override
    public void onClick(View view) {

    }
}