package shopkut.com.shopkut;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import constants.Constants;
import preference.SharedPref;
import utility.GpsTracker;
import utility.SingleVolley;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    CallbackManager callbackManager;
    static final Integer LOCATION = 0x1;

    SharedPref mPreferences;
    EditText edt_LOGINA_mobile, edt_LOGINA_password;
    TextView tv_LOGINA_forget_password;
    String password = "", mobile = "";
    Button bt_LOGINA_login, bt_LOGINA_create_new_account;
    TextView tv_LOGINA_guest_user;
    GoogleSignInAccount gss;
    GoogleApiClient googleApiClient;
    SignInButton signInButton_LOGINA;
    LoginButton loginButton_LOGINA;
    GoogleSignInOptions googleSignInOptions = null;
    static int i = 0, j = 0;
    static String fb_gender = "", fb_id = "", fb_last_name = "", url = "", phone = "";
    static String fetch_name = "", fetch_email = "", profile_pic = "";
    GoogleApiClient client;
    LocationRequest mLocationRequest;
    Location mLocation;
    LocationManager locationManager;
    double lat,lng;
    private GpsTracker gps;
    PendingResult<LocationSettingsResult> result;
    static final Integer GPS_SETTINGS = 0x7;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = new GoogleApiClient.Builder(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .build();
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION);
        askForGPS();
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login);
        mPreferences = SharedPref.getInstance(LoginActivity.this);

        gps = new GpsTracker(this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();

           // Toast.makeText(this, "Longitude:" + Double.toString(lng) + "\nLatitude:" + Double.toString(lat), Toast.LENGTH_SHORT).show();

        }




        callbackManager = CallbackManager.Factory.create();




        loginButton_LOGINA = (LoginButton) findViewById(R.id.bt_fb);
        edt_LOGINA_mobile = (EditText) findViewById(R.id.editTextMobile);
        edt_LOGINA_password = (EditText) findViewById(R.id.editTextpassword);
        tv_LOGINA_forget_password = (TextView) findViewById(R.id.tv_forgot_pwd);
        password = edt_LOGINA_password.getText().toString().trim();
        mobile = edt_LOGINA_mobile.getText().toString().trim();
        bt_LOGINA_login = (Button) findViewById(R.id.btn_login);
        bt_LOGINA_create_new_account = (Button) findViewById(R.id.btn_new_account);
        signInButton_LOGINA = (SignInButton) findViewById(R.id.google_btn);
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        tv_LOGINA_guest_user = (TextView) findViewById(R.id.txt_guest);
        loginButton_LOGINA.setReadPermissions("email");

        loginButton_LOGINA.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                setFacebookData(loginResult);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_LONG).show();

            }
        });

        tv_LOGINA_forget_password.setOnClickListener(this);
        bt_LOGINA_login.setOnClickListener(this);
        bt_LOGINA_create_new_account.setOnClickListener(this);
        signInButton_LOGINA.setOnClickListener(this);
        tv_LOGINA_guest_user.setOnClickListener(this);
        isValidate();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "shopkut.com.shopkut", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(LoginActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            //Toast.makeText(this, "GPS IS Enabled", Toast.LENGTH_SHORT).show();
        }

        //Toast.makeText(this, "GPS IS Enabled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                //Location
                case 1:
                    askForGPS();
                    break;

            }

            //Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        client.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        client.disconnect();
    }

    private void askForGPS() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(client, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(LoginActivity.this, GPS_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void setFacebookData(LoginResult loginResult) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email = response.getJSONObject().getString("email");
                    String first_name = response.getJSONObject().getString("first_name");
                    String last_name = response.getJSONObject().getString("last_name");
                    i = 1;
                    Toast.makeText(LoginActivity.this, "Welcome" + " " + first_name + " " + last_name, Toast.LENGTH_LONG).show();
                    mPreferences.setEmail(email);
                    mPreferences.setFacebookName(first_name + " " + last_name);

                    Profile profile = Profile.getCurrentProfile();
                    fb_id = profile.getId();
                    mPreferences.setFbId(fb_id);
                    mPreferences.setIsLogin(true);




                    fblogin();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,email,first_name,last_name,gender");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    public void fblogin() {
        url = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
        mPreferences.setOrigin("facebook");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FBLOGIN, new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONObject jsonObj1 = jsonObj.getJSONObject("user");
                            fetch_email = jsonObj1.getString("user_id");
                            mPreferences.setUserId(fetch_email);
                            String successmsg = jsonObj1.getString("status");
                            if (successmsg.equals("0") || successmsg.equals("1")) {
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                mPreferences.setLat(""+lat);
                                mPreferences.setLon(""+lng);
                                startActivity(i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", mPreferences.getFacebookName());
                params.put("email", mPreferences.getEmail());
                params.put("gender", fb_gender);
                params.put("profile_pic", "https://graph.facebook.com/" + fb_id + "/picture?type=large");
                params.put("origin", mPreferences.getOrigin());
                params.put("lat",""+lat);
                params.put("lon",""+lng);
                return params;


            }
        };

        SingleVolley.getmInstance(LoginActivity.this).addToRequestQue(stringRequest);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_new_account: {
                grashowSignUpActivity();
                break;
            }
            case R.id.btn_login: {

                if (isValidate())

                {
                    //{"user":{"status":"1","user_id":"6","name":"Mudit Srivastava","email":"mudit.srivastava1387@gmail.com","mobile":"9811600592","gender":"Male","latitude":"28.6258615","longitude":"77.3791638","last_login":"08\/17\/2017","success":"200","successmsg":"logged"}}
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                                        String str = jsonObject1.optString("successmsg").trim();
                                        String user_id = jsonObject1.optString("user_id");
                                        mPreferences.setUserId(user_id);
                                        if (str.equals("logged") || str.equals("wrong password")) {
                                            mPreferences.setLat(""+lat);
                                            mPreferences.setLon(""+lng);
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(intent);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        public Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("password", edt_LOGINA_password.getText().toString().trim());
                            params.put("mobile", edt_LOGINA_mobile.getText().toString().trim());
                            params.put("lat",""+lat);
                            params.put("lon",""+lng);
                            return params;
                        }


                    };

                    SingleVolley.getmInstance(LoginActivity.this).addToRequestQue(stringRequest);

                }

            }
            break;


            case R.id.tv_forgot_pwd: {
                showForgetPasswordActivity();
                break;
            }


            case R.id.google_btn: {

                Intent i = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(i, 6);

                break;
            }

            case R.id.txt_guest: {

                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                Toast.makeText(LoginActivity.this, "Welcome Guest", Toast.LENGTH_LONG).show();
                startActivity(i);
            }

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 6) {


            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {

        gss = result.getSignInAccount();
        Toast.makeText(this, "Welcome" + " " + gss.getDisplayName(), Toast.LENGTH_SHORT).show();
        mPreferences.setGoogleEmail(gss.getEmail());
        mPreferences.setGoogleName(gss.getDisplayName());
        j = 1;
        mPreferences.setGoogleUrl("" + gss.getPhotoUrl());
        mPreferences.setIsLogin(true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FBLOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONObject jsonObj1 = jsonObj.getJSONObject("user");
                            fetch_email = jsonObj1.getString("user_id");

                            mPreferences.setUserId(fetch_email);

                            String successmsg = jsonObj1.getString("status");
                            if (successmsg.equals("0") || successmsg.equals("1")) {
                                mPreferences.setLat(""+lat);
                                mPreferences.setLon(""+lng);
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", mPreferences.getGoogleName());
                params.put("email", mPreferences.getGoogleEmail());
                params.put("gender", "");
                params.put("profile_pic", "" + gss.getPhotoUrl());
                params.put("origin", "google");
                params.put("lat",""+lat);
                params.put("lon",""+lng);
                return params;
            }


        };

        SingleVolley.getmInstance(LoginActivity.this).addToRequestQue(stringRequest);

    }

    private void grashowSignUpActivity() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }


    public boolean isValidate() {

        if (!isValidMobile(edt_LOGINA_mobile.getText().toString())) {
            edt_LOGINA_mobile.requestFocus();
            edt_LOGINA_mobile.setError("Please Enter Mobile Number");
            return false;
        } else if (!isValidPassword(edt_LOGINA_password.getText().toString())) {
            edt_LOGINA_password.requestFocus();
            edt_LOGINA_password.setError("Please Enter Password");
            return false;
        }
        else {
            edt_LOGINA_mobile.setError(null);
            edt_LOGINA_password.setError(null);
        }
        return true;
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 5) {
            return true;
        }
        return false;
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void showForgetPasswordActivity() {
        Intent intent = new Intent(this, ForgetPassword.class);
        startActivity(intent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}






