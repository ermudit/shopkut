package shopkut.com.shopkut;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import adapter.DashBoardContentAdapter;
import constants.Constants;
import fragment.MainFragment;
import preference.SharedPref;
import utility.SingleVolley;

import static android.support.v4.view.KeyEventCompat.getKeyDispatcherState;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener, View.OnClickListener,
        TextView.OnEditorActionListener, AdapterView.OnItemClickListener {


    private DashBoardContentAdapter dashBoardContentAdapter;
    private RecyclerView recyclerView;
    int count = 0;
    com.github.clans.fab.FloatingActionButton actionButtonAddStore;
    FloatingActionMenu floatingActionMenu;
    GoogleApiClient client;
    static TextView tv_MAIN_ACTIVITYA_name, tv_MAIN_ACTIVITYA_email;
    public ImageView profile_img, goToStore;
    FrameLayout frameLayout;
    public TextView bt_MAIN_ACTIVITYA_user_my_profile;


    View header;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_CODE = 1;
    private String TAG = MainActivity.class.getSimpleName();
    private static ViewPager mPager;//mPager1,mPager2;
    private static int currentPage = 0;


    SharedPref mPreferences;
    MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        header = navigationView.getHeaderView(0);
        tv_MAIN_ACTIVITYA_name = (TextView) header.findViewById(R.id.txt_name);
        tv_MAIN_ACTIVITYA_email = (TextView) header.findViewById(R.id.text_email);
        profile_img = (ImageView) header.findViewById(R.id.imageView_profile);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        mPreferences = SharedPref.getInstance(MainActivity.this);
        application = new MyApplication();

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout, new MainFragment());
        ft.commit();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GETLOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONObject jsonObj1 = jsonObj.getJSONObject("user");
                            //Toast.makeText(MainActivity.this, "" + response, Toast.LENGTH_SHORT).show();
                            //Log.e("response", "" + response);
                            String fetch_name = jsonObj1.getString("name");
                            tv_MAIN_ACTIVITYA_name.setText(fetch_name);
                            String fetch_email = jsonObj1.getString("email");
                            tv_MAIN_ACTIVITYA_email.setText(fetch_email);

                            String profile_pic = jsonObj1.getString("profile_pic");
                            Glide.with(MainActivity.this).load(profile_pic).placeholder(R.mipmap.profile_pic).dontAnimate().into(profile_img);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPreferences.getUserId());
                params.put("origin", mPreferences.getOrigin());
                return params;
            }


        };


        SingleVolley.getmInstance(MainActivity.this).addToRequestQue(stringRequest);


    }


    @Override
    protected void onResume() {
        super.onResume();

        //Toast.makeText(MainActivity.this, "" + mPreferences.getUserId(), Toast.LENGTH_SHORT).show();

    }

    public void shareText(View view) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBodyText = "Your sharing message goes here";
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(intent, "Choose sharing method"));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.notification, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.notification) {


            return true;
        }

        if (id == R.id.search) {

            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {

            Intent i = new Intent(MainActivity.this, MainActivity.class);
            startActivity(i);

        }

        else if (id == R.id.nav_my_profile)
        {
            Intent i = new Intent(MainActivity.this, MyProfileActivity.class);
            startActivity(i);
        }
        else if (id == R.id.nav_offer_zone)
        {

        }
        else if (id == R.id.nav_my_store) {


        } else if (id == R.id.nav_my_orders) {


        } else if (id == R.id.nav_my_chat) {
            Intent i = new Intent(MainActivity.this, MyChatActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_ask_for_store) {


        } else if (id == R.id.nav_sell) {

        } else if (id == R.id.nav_rate_us) {
            /*Intent intent = new Intent(this, RateActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();*/

        } else if (id == R.id.nav_share) {

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBodyText = "Check it out. Your message goes here";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
            startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
            return true;
        } else if (id == R.id.nav_help_support) {


        } else if (id == R.id.nav_setting) {


        } else if (id == R.id.nav_logout) {
            mPreferences.clearPreferences();
            LoginManager.getInstance().logOut();
            LoginActivity.i = 0;
            LoginActivity.j = 0;
            mPreferences.setIsLogin(false);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGOUT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(MainActivity.this, "" + response, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });


            SingleVolley.getmInstance(MainActivity.this).addToRequestQue(stringRequest);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        /// Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.float_add:

                break;


        }


    }



}
