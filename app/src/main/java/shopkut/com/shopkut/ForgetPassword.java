package shopkut.com.shopkut;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import constants.Constants;
import utility.SingleVolley;

public class ForgetPassword extends AppCompatActivity {


    EditText ed_FORGET_PASSWORDA_email;
    Button bt_FORGET_PASSWORDA_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        initViews();
        isValidate();
    }

    private void initViews() {

        ed_FORGET_PASSWORDA_email = (EditText) findViewById(R.id.editTextpassword);
        bt_FORGET_PASSWORDA_submit = (Button) findViewById(R.id.forgetPasswordbtn);
        bt_FORGET_PASSWORDA_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FORGET,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                                    String status = jsonObject1.getString("status");
                                    String success_msg = jsonObject1.getString("successmsg").trim();

                                    //  {"user":{"status":"0","type":"android","error":"201","errormsg":"User already exist"}}

                                    if (success_msg.equals("Invalid email") || success_msg.equals("Mail Sent Successfully"))

                                    {
                                        Toast.makeText(ForgetPassword.this, "" + success_msg, Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(ForgetPassword.this, OtpVerificationActivity.class);
                                        startActivity(i);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", ed_FORGET_PASSWORDA_email.getText().toString().trim());
                        return params;


                    }


                };

                SingleVolley.getmInstance(ForgetPassword.this).addToRequestQue(stringRequest);
            }
        });
    }

    public boolean isValidate() {

        if (!isValidEmail(ed_FORGET_PASSWORDA_email.getText().toString())) {
            ed_FORGET_PASSWORDA_email.requestFocus();
            ed_FORGET_PASSWORDA_email.setError("Please Enter Email");
            return false;
        } else {
            ed_FORGET_PASSWORDA_email.setError(null);

        }

        return true;
    }

    private boolean isValidEmail(String emailAddress) {
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }


    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                break;
            }
        }
        return true;
    }


}
