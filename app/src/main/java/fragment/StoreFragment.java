package fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.StoreContentAdapter;
import model.Image;
import model.Store_Data;
import preference.SharedPref;
import shopkut.com.shopkut.R;
import utility.SingleVolley;
import static com.facebook.FacebookSdk.getApplicationContext;


public class StoreFragment extends Fragment {

    View mView;
    RecyclerView recyclerview;
    StringRequest stringRequest1;
    List<Store_Data> storeData_models;
    List<Image> images;
    static String url = "";
    private StoreContentAdapter storeContentAdapter;
    TextView store_name, store_address;
    FragmentManager fragmentManager;
    SharedPref mPreferences;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.store_fragment, container, false);


        store_name = (TextView) mView.findViewById(R.id.store_name);
        store_address = (TextView) mView.findViewById(R.id.store_address);
        recyclerview = (RecyclerView) mView.findViewById(R.id.recycler_view);
        storeData_models = new ArrayList<>();
        images = new ArrayList<>();
        fragmentManager = getActivity().getSupportFragmentManager();

        mPreferences = SharedPref.getInstance(getApplicationContext());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.getString("url", "no String");
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONArray jsonArray = jsonObj.getJSONArray("all_shops");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObj1 = jsonArray.getJSONObject(i);
                                Store_Data store_data = new Store_Data();
                                store_data.setShop_id(jsonObj1.getString("shop_id"));

                                store_data.setShop_name(jsonObj1.getString("shop_name"));
                                store_data.setShop_address(jsonObj1.getString("shop_address"));
                                store_data.setShop_rate(jsonObj1.getString("shop_rate"));
                                store_data.setTotal_rating(jsonObj1.getString("total_rating"));
                                JSONObject jsonObject = jsonObj1.getJSONObject("shop_slider");
                                JSONArray jsonArray1 = jsonObject.getJSONArray("images");
                                for(int j=0;j<jsonArray1.length();j++)
                                {
                                    Image storeData = new Image();
                                    storeData.setName(jsonArray1.getString(j));
                                    images.add(storeData);
                                }
                                storeData_models.add(store_data);
                                storeContentAdapter = new StoreContentAdapter(getApplicationContext(),storeData_models,images,fragmentManager);
                                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                                recyclerview.setLayoutManager(mLayoutManager);
                                recyclerview.setItemAnimator(new DefaultItemAnimator());
                                recyclerview.setNestedScrollingEnabled(false);
                                recyclerview.setAdapter(storeContentAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest);


        return mView;
    }



}
