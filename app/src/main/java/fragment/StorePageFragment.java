package fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import constants.Constants;
import model.Image;
import model.Product_Image;
import model.Reviews;
import preference.SharedPref;
import shopkut.com.shopkut.MainActivity;
import shopkut.com.shopkut.R;
import utility.SingleVolley;

import static com.facebook.FacebookSdk.getApplicationContext;


public class StorePageFragment extends Fragment {

     String shop_id;
    View mView;
    RequestQueue requestQueue;
    TextView tv_text;
    //TextView tv_rating;
    TextView tv_rating_total;
    TextView tv_we_the_first;
    TextView all_reviews;
    RatingBar tv_rating;
    SharedPref mPreferences;
    MapView mMapView;
    private GoogleMap googleMap;
    static String latitude, longitude, shopid;
    ImageButton img_arrow;
    FragmentManager fragmentManager;
    static int review_length;
    static ArrayList<Reviews> arr_reviews;
    static ArrayList<Product_Image> arr_images;
    ScrollView scrollView;
    TextView auto_complete_textview;
    ArrayList<String> company_names,detail_product;
    ArrayAdapter<String> adapter;
    ImageView img_arrow1;

    public StorePageFragment(FragmentManager fragment) {
        fragmentManager = fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.activity_store_page, container, false);
        tv_text = (TextView) mView.findViewById(R.id.tv_text);
       /* tv_rating = (TextView) mView.findViewById(R.id.txt_rating);*/
        tv_rating=(RatingBar) mView.findViewById(R.id.txt_rating);

        tv_rating_total = (TextView) mView.findViewById(R.id.txt_rating1);
        mMapView = (MapView) mView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        img_arrow = (ImageButton) mView.findViewById(R.id.img_arrow);
        tv_we_the_first = (TextView) mView.findViewById(R.id.tv_we_the_first);
        tv_we_the_first.setText(R.string.Review_Here);
        all_reviews = (TextView) mView.findViewById(R.id.all_reviews);
       // recyclerview = (RecyclerView) mView.findViewById(R.id.recycler_view_product);
        img_arrow1 = (ImageView) mView.findViewById(R.id.img_arrow1);
        auto_complete_textview = (TextView) mView.findViewById(R.id.auto_complete_textview);
        arr_reviews = new ArrayList<>();
        arr_images = new ArrayList<>();
        mPreferences = SharedPref.getInstance(getApplicationContext());
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        scrollView = (ScrollView) mView.findViewById(R.id.scrollableContents);
        company_names = new ArrayList<>();
        detail_product = new ArrayList<>();








        Bundle bundle = this.getArguments();
        if (bundle != null) {
            shop_id = bundle.getString("shop_id", "no String");

        }


        tv_we_the_first.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    tv_we_the_first.setText("");
            }
        });


        StringRequest stringRequest = new StringRequest(Request.Method.POST, shop_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(getApplicationContext(), ""+response, Toast.LENGTH_SHORT).show();
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONObject jsonObj1 = jsonObj.getJSONObject("shopkeeper");
                            String shop_name = jsonObj1.getString("shop_name");
                            shopid = jsonObj1.getString("shop_id");
                            String shop_address = jsonObj1.getString("shop_address");
                            String pin_code = jsonObj1.getString("pin_code");
                            String city = jsonObj1.getString("city");
                            String state = jsonObj1.getString("state");
                            latitude = jsonObj1.getString("latitude");
                            longitude = jsonObj1.getString("longitude");
                            tv_text.setText(shop_name + "\n" + shop_address + " " + "," + " " + pin_code + "\n" + city + " " + "," + " " + state);
                            String shop_avg_rating = jsonObj1.getString("shop_avg_rating");
                            tv_rating.setRating(Float.valueOf(shop_avg_rating));
                            String shop_total_rating = jsonObj1.getString("shop_total_rating");
                            tv_rating_total.setText(shop_total_rating);


                            mMapView.onResume(); // needed to get the map to display immediately

                            try {
                                MapsInitializer.initialize(getActivity().getApplicationContext());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            mMapView.getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(GoogleMap mMap) {
                                    googleMap = mMap;

                                    // For showing a move to my location button
                                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling
                                        //    ActivityCompat#requestPermissions
                                        // here to request the missing permissions, and then overriding
                                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                        //                                          int[] grantResults)
                                        // to handle the case where the user grants the permission. See the documentation
                                        // for ActivityCompat#requestPermissions for more details.
                                        return;
                                    }
                                    googleMap.setMyLocationEnabled(true);

                                    // For dropping a marker at a point on the Map
                                    LatLng sydney = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
                                    googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

                                    // For zooming automatically to the location of the marker
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                }
                            });


                            JSONArray jsonArray = jsonObj.getJSONArray("product");
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String product_company = jsonObject.getString("product_company");
                                String product_id = jsonObject.getString("product_id");
                                company_names.add(product_company);
                                detail_product.add(product_id);
                            }


                            img_arrow1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    showDialog(company_names,detail_product);
                                }
                            });



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest);



        img_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringRequest stringReq = new StringRequest(Request.Method.POST, Constants.SHOP_REVIEW,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObj = new JSONObject(response);
                                    JSONObject jsonObj1 = jsonObj.getJSONObject("user");
                                    String msg = jsonObj1.getString("successmsg");
                                    Toast.makeText(getApplicationContext(), ""+msg, Toast.LENGTH_SHORT).show();
                                    tv_we_the_first.setText("");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("shopid", shopid);
                        params.put("userid", mPreferences.getUserId());
                        params.put("review",tv_we_the_first.getText().toString());
                        return params;
                    }

                };

                SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringReq);

            }
        });


        all_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();
                ReviewFragment fragment = new ReviewFragment(review_length);
                Bundle bundle = new Bundle();
                bundle.putString("shop_id", shop_id);
                bundle.putString("shopid",shopid);
                fragment.setArguments(bundle);
                Changing_Fragment(fragmentManager,fragment);
            }
        });


        return mView;
    }

    private void showDialog(ArrayList product_company, final ArrayList detail_product) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext(), R.style.DialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.customdialogwithlist_layout, null);
        dialogBuilder.setView(dialogView);

        final ListView listView = (ListView) dialogView.findViewById(R.id.listview);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, product_company);

        listView.setAdapter(adapter);

        final AlertDialog alertDialog = dialogBuilder.create();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition  = position;

                // ListView Clicked item value
                String  itemValue  = (String) listView.getItemAtPosition(position);


                StringRequest stringReq = new StringRequest(Request.Method.POST, ""+detail_product.get(itemPosition),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Show Alert
                                Toast.makeText(getApplicationContext(), "" + response , Toast.LENGTH_LONG).show();
                                ProductListFragment fragment = new ProductListFragment();
                                alertDialog.hide();
                                Changing_Fragment(fragmentManager,fragment);

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

                SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringReq);

            }

        });


        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        ImageView cancel_btn = (ImageView) dialogView.findViewById(R.id.cross_button);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });
        alertDialog.show();
    }


    public void Changing_Fragment(FragmentManager fragmentManager, Fragment fragment) {
        // TODO

        String fragmentTagName = fragment.getClass().getName();
        System.out.println("Fragment Tag Name : " + fragmentTagName);
        FragmentManager manager = fragmentManager;
        boolean fragmentPopped = manager.popBackStackImmediate(fragmentTagName, 0);

        if (!fragmentPopped
                && manager.findFragmentByTag(fragmentTagName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTagName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(fragmentTagName);
            ft.commit();

        }
    }
}


