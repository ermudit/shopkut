package fragment;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionMenu;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import adapter.DashBoardContentAdapter;
import adapter.MainFragment_Adapter;
import constants.Constants;
import me.relex.circleindicator.CircleIndicator;
import model.Dashboard_category;
import preference.SharedPref;
import shopkut.com.shopkut.MainActivity;
import shopkut.com.shopkut.R;
import utility.SingleVolley;

import static com.facebook.FacebookSdk.getApplicationContext;
public class MainFragment extends Fragment implements View.OnClickListener {





    View mView;
    com.github.clans.fab.FloatingActionButton actionButtonAddStore;
    FloatingActionMenu floatingActionMenu;
    ViewPager viewPager;
    static ArrayList<String> arr_image;
    static ArrayList<Dashboard_category> arr_text, cat_info;
    RecyclerView recyclerview;
    private ArrayList<Dashboard_category> ImageList;
    private DashBoardContentAdapter dashBoardContentAdapter;
    public static String url_link = "";
    FragmentManager fragmentManager;
    SharedPref mPreferences;


    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.main_fragment_layout, container, false);

        floatingActionMenu = (FloatingActionMenu) mView.findViewById(R.id.floating_action_menu);
        actionButtonAddStore = (com.github.clans.fab.FloatingActionButton) mView.findViewById(R.id.float_add);
        viewPager = (ViewPager) mView.findViewById(R.id.pager);
        arr_image = new ArrayList<String>();
        arr_text = new ArrayList<>();
        cat_info = new ArrayList<>();
        recyclerview = (RecyclerView) mView.findViewById(R.id.recycler_view);
        fragmentManager = getActivity().getSupportFragmentManager();
        mPreferences = SharedPref.getInstance(getApplicationContext());
        ImageList = new ArrayList<Dashboard_category>();
        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Constants.CATEGORYLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);

                            JSONArray jsonArray = jsonObj.getJSONArray("Category");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                Dashboard_category dashboard_category = new Dashboard_category();
                                String cat_name = json.getString("cat_name");
                                String cat_icon = json.getString("cat_icon");
                                url_link = json.getString("url_link");
                                dashboard_category.setCat_name(json.getString("cat_name"));
                                dashboard_category.setCat_icon(json.getString("cat_icon"));
                                dashboard_category.setUrl_link(json.getString("url_link"));
                                arr_text.add(dashboard_category);
                                ImageList.add(dashboard_category);
                                cat_info.add(dashboard_category);
                                dashBoardContentAdapter = new DashBoardContentAdapter(getApplicationContext(), ImageList, arr_text, cat_info,fragmentManager);
                                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                                recyclerview.setLayoutManager(mLayoutManager);
                                recyclerview.setItemAnimator(new DefaultItemAnimator());
                                recyclerview.setAdapter(dashBoardContentAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("latitude",mPreferences.getLat());
                params.put("longitude", mPreferences.getLon());
                return params;

            }
        };



        SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest1);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.IMAGESLIDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            JSONObject jsonObj1 = jsonObj.getJSONObject("Slider_images");
                            JSONArray jsonArray = jsonObj1.getJSONArray("image_location");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                arr_image.add(jsonArray.getString(i));
                                MainFragment_Adapter myadapter = new MainFragment_Adapter(getApplicationContext(), arr_image);
                                myadapter.notifyDataSetChanged();
                                viewPager.setAdapter(myadapter);
                                CircleIndicator indicator = (CircleIndicator) mView.findViewById(R.id.indicator);
                                indicator.setViewPager(viewPager);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest);
       // Toast.makeText(getApplicationContext(), "" + arr_image.size(), Toast.LENGTH_SHORT).show();
        actionButtonAddStore.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.float_add:
               // Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();
                break;


        }
    }


}
