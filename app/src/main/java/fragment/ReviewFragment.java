package fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import adapter.ReviewAdapter;
import constants.Constants;
import model.Dashboard_category;
import model.Reviews;
import preference.SharedPref;
import shopkut.com.shopkut.MainActivity;
import shopkut.com.shopkut.MyProfileActivity;
import shopkut.com.shopkut.R;
import utility.SingleVolley;

import static android.R.id.toggle;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin on 9/8/2017.
 */

@SuppressLint("ValidFragment")
public class ReviewFragment extends Fragment {


    RecyclerView recyclerview;
    Toolbar toolbar;
    ReviewAdapter reviewAdapter;
    String shop_id,shopid,name,profile_pic;
    int review_length;
    static ArrayList<Reviews> arr_reviews,arr_user_id,arr_review_date,arr_review_time;
    SharedPref mPreferences;
    static StringRequest request;
    TextView no_review_txt;


    @SuppressLint("ValidFragment")
    ReviewFragment(int review_length)
    {
        this.review_length = review_length;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.review_frag,container,false);
        getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        recyclerview = (RecyclerView) mView.findViewById(R.id.recycler_view1);
        getActivity().setTitle("Review");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);

        arr_reviews = new ArrayList<>();
        arr_user_id = new ArrayList<>();
        arr_review_time = new ArrayList<>();
        arr_review_date =  new ArrayList<>();
        mPreferences = SharedPref.getInstance(getApplicationContext());
        no_review_txt = (TextView) mView.findViewById(R.id.no_review_text);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            shop_id = bundle.getString("shop_id", "no String");
            shopid = bundle.getString("shopid","no Stringg");
        }



      StringRequest stringRequest = new StringRequest(Request.Method.POST, shop_id,
              new Response.Listener<String>() {
                  @Override
                  public void onResponse(String response) {
                      try {
                          JSONObject jsonObj = new JSONObject(response);
                          int counter = jsonObj.getInt("counter");


                          if(counter == 0)
                          {
                             recyclerview.setVisibility(View.GONE);
                              no_review_txt.setVisibility(View.VISIBLE);
                          }
                         else {
                              JSONArray jsonArray = jsonObj.getJSONArray("all_reviews");
                              review_length = jsonArray.length();
                              for (int k = 0; k < jsonArray.length(); k++) {
                                  JSONObject jsonObject = jsonArray.getJSONObject(k);
                                  String str = jsonObject.getString("review_stmt");
                                  String user_id = jsonObject.getString("user_id");
                                  String review_date = jsonObject.getString("review_date");
                                  String review_time = jsonObject.getString("review_time");
                                  Reviews reviews = new Reviews();
                                  reviews.setMsg(str);
                                  reviews.setUser_id(user_id);
                                  reviews.setReview_date(review_date);
                                  reviews.setReview_time(review_time);
                                  arr_reviews.add(reviews);
                                  arr_user_id.add(reviews);
                                  arr_review_date.add(reviews);
                                  arr_review_time.add(reviews);
                                  // getName(arr_user_id);

                                  reviewAdapter = new ReviewAdapter(shop_id, shopid, review_length, arr_reviews, arr_user_id, arr_review_date, arr_review_time);
                                  RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                  recyclerview.setLayoutManager(mLayoutManager);

                                  recyclerview.setItemAnimator(new DefaultItemAnimator());
                                  recyclerview.setAdapter(reviewAdapter);

                              }
                          }
                      } catch (JSONException e) {
                          e.printStackTrace();
                      }


                  }
              }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {

          }
      });


        SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest);


        return mView;


    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.notification);

        item.setVisible(false);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {



                break;
            }
        }
        return true;
    }





}
