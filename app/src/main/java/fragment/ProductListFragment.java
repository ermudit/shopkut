package fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adapter.ProductImageAdapter;
import shopkut.com.shopkut.R;


/**
 * Created by Admin on 9/14/2017.
 */

public class ProductListFragment extends Fragment {

     RecyclerView recyclerview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.product_list_fragment,container,false);
        recyclerview = (RecyclerView) mView.findViewById(R.id.recycler_view);
        ProductImageAdapter productImageAdapter = new ProductImageAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(productImageAdapter);
        return mView;
    }



}
