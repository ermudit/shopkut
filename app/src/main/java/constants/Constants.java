package constants;


public class Constants {


    public static final String SIGNUP="http://mymarketapp.000webhostapp.com/myApp/user_sign_up_api";
    public static final String FBLOGIN="http://mymarketapp.000webhostapp.com/myApp/facebook_signup_api";
    public static final String GETLOGIN = "http://mymarketapp.000webhostapp.com/myApp/facebook_user_api";
    public static final String LOGOUT = "http://mymarketapp.000webhostapp.com/myApp/logout_api";
    public static final String IMAGESLIDER = "http://mymarketapp.000webhostapp.com/myApp/image_slider_api";
    public static final String CATEGORYLIST = "http://mymarketapp.000webhostapp.com/myApp/category_api";
    public static final String LOGIN ="http://mymarketapp.000webhostapp.com/myApp/user_login_api";
    public static final String FORGET="http://mymarketapp.000webhostapp.com/myApp/user_forgot_password_api";
    public static final String RESET_PASSWORD="http://mymarketapp.000webhostapp.com/myApp/reset_password_api";
    public static final String USER_EDIT_PROFILE="http://mymarketapp.000webhostapp.com/myApp/user_edit_api";
    public static final String SHOP_REVIEW = "http://mymarketapp.000webhostapp.com/myApp/add_shop_review_api";

}
