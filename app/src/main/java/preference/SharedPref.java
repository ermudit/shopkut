package preference;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPref {


    private static SharedPreferences msharedPreferences;
    private static SharedPref msharedPref;
    private SharedPreferences.Editor editor;


    public SharedPref(Context context) {
        msharedPreferences = context.getSharedPreferences("sharedpref", Context.MODE_PRIVATE);
        editor = msharedPreferences.edit();
    }

    public static final SharedPref getInstance(Context context) {
        if (msharedPref == null) {
            msharedPref = new SharedPref(context);
        }
        return msharedPref;
    }


    public String getEmail() {
        return msharedPreferences.getString("email", "Email");

    }

    public void setEmail(String email) {
        editor.putString("email", email);
        editor.commit();
    }


    public void setFbId(String id) {
        editor.putString("id", id);
        editor.commit();
    }


    public void setOrigin(String origin) {
        editor.putString("origin", origin);
        editor.commit();
    }


    public String getOrigin() {
        return msharedPreferences.getString("origin", "null origin");
    }

    public String getFbId() {
        return msharedPreferences.getString("id", "0");

    }

    public void setGoogleUrl(String url) {
        editor.putString("url", url);
        editor.commit();
    }

    public String getGoogleUrl() {
        return msharedPreferences.getString("url", "0");

    }


    public void setUserId(String url) {
        editor.putString("user_id", url);
        editor.commit();
    }

    public String getUserId() {
        return msharedPreferences.getString("user_id", "id");

    }


    public void setGoogleEmail(String email) {
        editor.putString("gmail", email);
        editor.commit();
    }

    public String getGoogleEmail() {
        return msharedPreferences.getString("gmail", "Email");

    }

    public void setGoogleName(String name) {
        editor.putString("gname", name);
        editor.commit();
    }

    public String getGoogleName() {
        return msharedPreferences.getString("gname", "Name");

    }


    public void setFacebookName(String name) {
        editor.putString("fbname", name);
        editor.commit();
    }


    public String getFacebookName() {
        return msharedPreferences.getString("fbname", "Name");

    }


    public void setIsLogin(boolean login) {
        editor.putBoolean("loginstatus", login);
        editor.commit();
    }

    public boolean getIsLogin() {
        return msharedPreferences.getBoolean("loginstatus", false);

    }

    public void setLat(String login) {
        editor.putString("lat", login);
        editor.commit();
    }

    public String getLat() {
        return msharedPreferences.getString("lat", "lat null");

    }


    public void setLon(String login) {
        editor.putString("lon", login);
        editor.commit();
    }

    public String getLon() {
        return msharedPreferences.getString("lon", "lon null");

    }

    public void clearPreferences() {
        editor.clear();
        editor.commit();
    }

}
