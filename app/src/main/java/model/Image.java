package model;

import java.io.Serializable;


public class Image implements Serializable {
    private String name;
    private int thumbnail_gall;
//    private String small, medium, large;
//    private String timestamp;

    public int getThumbnail_gall() {
        return thumbnail_gall;
    }

//    public void setThumbnail_gall(int thumbnail_gall) {
//        this.thumbnail_gall = thumbnail_gall;
//    }

    public Image(String name, int thumbnail) {
        this.name = name;
        this.thumbnail_gall = thumbnail;
    }


    public Image() {
    }

    public Image(int thumbnail_gallery) {
        this.thumbnail_gall = thumbnail_gallery;
    }

//    public Image(String name, String small, String medium, String large, String timestamp) {
//        this.name = name;
//        this.small = small;
//        this.medium = medium;
//        this.large = large;
//        this.timestamp = timestamp;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getSmall() {
//        return small;
//    }
//
//    public void setSmall(String small) {
//        this.small = small;
//    }
//
//    public String getMedium() {
//        return medium;
//    }
//
//    public void setMedium(String medium) {
//        this.medium = medium;
//    }
//
//    public String getLarge() {
//        return large;
//    }
////
//    public void setLarge(String large) {
//        this.large = large;
//    }
//
//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }
}
