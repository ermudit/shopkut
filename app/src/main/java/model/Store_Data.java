package model;


public class Store_Data {
    private String shop_id;
    private String shop_name;
    private String shop_address;
    private String pin_code;
    private String city;
    private String state;
    private String country;
    private String shop_geo_location;
    private String shop_rate;
    private String total_rating;

    public String getShop_slider() {
        return shop_slider;
    }

    public void setShop_slider(String shop_slider) {
        this.shop_slider = shop_slider;
    }

    private String shop_slider;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getShop_geo_location() {
        return shop_geo_location;
    }

    public void setShop_geo_location(String shop_geo_location) {
        this.shop_geo_location = shop_geo_location;
    }

    public String getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(String shop_rate) {
        this.shop_rate = shop_rate;
    }

    public String getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(String total_rating) {
        this.total_rating = total_rating;
    }
}
