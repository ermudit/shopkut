package adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import model.Image;
import model.Product_Image;
import shopkut.com.shopkut.R;

/**
 * Created by Admin on 9/12/2017.
 */

public class ProductImageAdapter extends RecyclerView.Adapter<ProductImageAdapter.MyViewHolder> {


    private ArrayList<Product_Image> arr_images;
    private Context mContext;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_image_adapter,parent,false);
        return new MyViewHolder(mView);
    }


    public ProductImageAdapter()
    {
       /* this.arr_images = arr_images;
        this.mContext = mContext;*/
    }


    @Override
    public void onBindViewHolder(ProductImageAdapter.MyViewHolder holder, int position) {



    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout.LayoutParams lp;
        LinearLayout ll;
        LinearLayout lin_product_image;

        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }
}
