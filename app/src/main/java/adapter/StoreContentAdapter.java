package adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import fragment.StorePageFragment;
import model.Image;
import model.Store_Data;
import shopkut.com.shopkut.R;


public class StoreContentAdapter extends RecyclerView.Adapter<StoreContentAdapter.MyViewHolder> {

    private List<Store_Data> storeData_model;
    private List<Image> images;
    private Context mContext;
    FragmentManager fragmentManager;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView store_Name;
        public TextView store_Address;
        public CheckBox like_unlike;
        public TextView store_rating;
        public TextView store_no_of_rating;
        RelativeLayout relativeLayout;
        ImageView image1, image2, image3;
        String shop_slider;
        Button go_to_store;
        TextView tv;
        LinearLayout.LayoutParams lp;
        LinearLayout ll;

        public MyViewHolder(View view) {
            super(view);

            Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/CaviarDreams_Bold.ttf");

            store_Name = (TextView) view.findViewById(R.id.store_name);
            go_to_store = (Button) view.findViewById(R.id.go_to_store);

            store_Address = (TextView) view.findViewById(R.id.store_address);
            store_rating = (TextView) view.findViewById(R.id.store_rating);
            store_no_of_rating = (TextView) view.findViewById(R.id.store_no_of_rating);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative);

            ll = new LinearLayout(mContext);
            ll.setOrientation(LinearLayout.HORIZONTAL);

            // Configuring the width and height of the linear layout.
            LinearLayout.LayoutParams llLP = new LinearLayout.LayoutParams(
                    //android:layout_width="match_parent" an in xml
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    //android:layout_height="wrap_content"
                    LinearLayout.LayoutParams.MATCH_PARENT);
            llLP.width = 630;
            llLP.height = 200;
            ll.setLayoutParams(llLP);
            store_Name.setTypeface(tf);
            store_Address.setTypeface(tf);
            store_rating.setTypeface(tf);
            store_no_of_rating.setTypeface(tf);

        }
    }

    public StoreContentAdapter(Context context, List<Store_Data> storeData_model, List<Image> images,FragmentManager fragmentManager) {
        mContext = context;
        this.storeData_model = storeData_model;
        this.images = images;
        this.fragmentManager = fragmentManager;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.store_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Store_Data store_model = storeData_model.get(position);

        holder.store_Name.setText(store_model.getShop_name());
        holder.store_Address.setText(store_model.getShop_address());
        holder.store_rating.setText(store_model.getShop_rate());
        holder.store_no_of_rating.setText(store_model.getTotal_rating());


        store_model.getShop_id();

        for(int i=0;i<images.size();i++)
        {
            Image image = images.get(i);
            ImageView image1 = new ImageView(mContext);
            holder.lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            holder.lp.width = 200;
            holder.lp.height = 200;
            //  image1.setImageResource(R.mipmap.chat_img);

            image1.setLayoutParams(holder.lp);
            // image1.setPadding(0,8, 0, 8);
            holder.ll.addView(image1);
            holder.relativeLayout.setBackgroundColor(Color.GREEN);
            Glide.with(mContext).load(image.getName()).into(image1);
            holder.relativeLayout.removeAllViews();
            holder.relativeLayout.addView(holder.ll);



        }


        holder.go_to_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StorePageFragment store_fragment = new StorePageFragment(fragmentManager);
                Bundle bundle = new Bundle();
                bundle.putString("shop_id", store_model.getShop_id());
                store_fragment.setArguments(bundle);


                Changing_Fragment(fragmentManager,store_fragment);
            }
        });

    }


    public void Changing_Fragment(FragmentManager fragmentManager, Fragment fragment) {
        // TODO

        String fragmentTagName = fragment.getClass().getName();
        System.out.println("Fragment Tag Name : " + fragmentTagName);
        FragmentManager manager = fragmentManager;
        boolean fragmentPopped = manager.popBackStackImmediate(fragmentTagName,0);

        if (!fragmentPopped
                && manager.findFragmentByTag(fragmentTagName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTagName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(fragmentTagName);
            ft.commit();

        }

    }


    @Override
    public int getItemCount() {
        return storeData_model.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}