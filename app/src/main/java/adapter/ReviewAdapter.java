package adapter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import constants.Constants;
import model.Dashboard_category;
import model.Reviews;
import preference.SharedPref;
import shopkut.com.shopkut.R;
import utility.SingleVolley;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin on 9/8/2017.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    String shop_id,showid;
    SharedPref mPreferences;
    static int length=-1;
    int review_length;
    ArrayList<Reviews> arr_reviews,arr_user_id,arr_review_time,arr_review_date;
   static String name,profile_pic;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_adapter,parent,false);


        return new MyViewHolder(mView);
    }

    public ReviewAdapter(String shop_id, String shopid, int review_length, ArrayList<Reviews> arr_reviews,ArrayList<Reviews> arr_user_id
     ,ArrayList<Reviews> arr_review_date,ArrayList<Reviews> arr_review_time)
    {
        this.shop_id = shop_id;
        this.showid = shopid;
        this.review_length = review_length;
        this.arr_reviews = arr_reviews;
        this.arr_user_id = arr_user_id;
        this.arr_review_date = arr_review_date;
        this.arr_review_time = arr_review_time;
    }

    public ReviewAdapter()
    {

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {






        Reviews txt = arr_reviews.get(position);
        final Reviews userid = arr_user_id.get(position);
        Reviews review_time = arr_review_time.get(position);
        Reviews review_date = arr_review_date.get(position);
       for(int i=0;i<arr_reviews.size();i++)
       {
           holder.reviewer_text.setText(txt.getMsg());
       }


       holder.cardView.setCardBackgroundColor(Color.WHITE);
       for(int j=0;j<arr_user_id.size();j++) {
           StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GETLOGIN,
                   new Response.Listener<String>() {
                       @Override
                       public void onResponse(String response) {
                           try {
                               JSONObject jsonObject = new JSONObject(response);
                               JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                               name = jsonObject1.getString("name");
                               profile_pic = jsonObject1.getString("profile_pic");
                               holder.reviewer_name.setText(name);

                               Glide.with(getApplicationContext()).load(profile_pic).into(holder.imageView);
                           } catch (JSONException e) {
                               e.printStackTrace();
                           }

                       }
                   }, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {

               }
           }) {
               public Map<String, String> getParams() {
                   Map<String, String> params = new HashMap<String, String>();
                   params.put("user_id", userid.getUser_id());
                   return params;
               }

           };

           SingleVolley.getmInstance(getApplicationContext()).addToRequestQue(stringRequest);
       }

       for(int i=0;i<arr_review_date.size();i++)
       {
           holder.review_date.setText(review_date.getReview_date());}

        for(int i=0;i<arr_review_time.size();i++)
        {
            holder.review_time.setText(review_time.getReview_time());
        }

    }

    @Override
    public int getItemCount() {
        return review_length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView reviewer_text,reviewer_name,review_date,review_time;
        LinearLayout linearLayout;
        ImageView imageView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mPreferences = SharedPref.getInstance(getApplicationContext());
            linearLayout = (LinearLayout) itemView.findViewById(R.id.lin_review);
            reviewer_name = (TextView) itemView.findViewById(R.id.reviewer_name);
            reviewer_text = (TextView) itemView.findViewById(R.id.reviewer_text);
            imageView = (ImageView) itemView.findViewById(R.id.review_img_profile);
            cardView = (CardView) itemView.findViewById(R.id.review_card);
            review_date = (TextView) itemView.findViewById(R.id.review_date);
            review_time = (TextView) itemView.findViewById(R.id.review_time);
        }
    }
}
