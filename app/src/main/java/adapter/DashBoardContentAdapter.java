package adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import fragment.StoreFragment;
import model.Dashboard_category;
import shopkut.com.shopkut.R;


public class DashBoardContentAdapter extends RecyclerView.Adapter<DashBoardContentAdapter.MyViewHolder> {

    private ArrayList<Dashboard_category> images;
    private ArrayList<Dashboard_category> countryList, cat_info;
    private Context mContext;
    FragmentManager fragmentManager;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView cat_Image;
        public TextView cat_Name;

        public MyViewHolder(View view) {
            super(view);
            cat_Image = (ImageView) view.findViewById(R.id.cat_image);

            Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/CaviarDreams_Bold.ttf");

            cat_Name = (TextView) view.findViewById(R.id.cat_name);
            cat_Name.setTypeface(tf);
            cat_Name.setSelected(true);
        }
    }


    public DashBoardContentAdapter(Context context, ArrayList<Dashboard_category> images, ArrayList<Dashboard_category> text, ArrayList<Dashboard_category> cat_info,FragmentManager fragmentManager) {
        mContext = context;
        this.images = images;
        this.countryList = text;
        this.cat_info = cat_info;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_categories, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Dashboard_category txt = countryList.get(position);
        Dashboard_category icon = images.get(position);

        holder.cat_Name.setText(txt.getCat_name());

        Glide.with(mContext).load(icon.getCat_icon()).into(holder.cat_Image);


        holder.cat_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Dashboard_category url_link = cat_info.get(position);
                StoreFragment fragment = new StoreFragment();
                Bundle bundle = new Bundle();
                bundle.putString("url", url_link.getUrl_link());
                fragment.setArguments(bundle);

                Changing_Fragment(fragmentManager,fragment);


            }
        });

    }


    public void Changing_Fragment(FragmentManager fragmentManager, Fragment fragment) {
        // TODO

        String fragmentTagName = fragment.getClass().getName();
        System.out.println("Fragment Tag Name : " + fragmentTagName);
        FragmentManager manager = fragmentManager;
        boolean fragmentPopped = manager.popBackStackImmediate(fragmentTagName,0);

        if (!fragmentPopped
                && manager.findFragmentByTag(fragmentTagName) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTagName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(fragmentTagName);
            ft.commit();

        }

    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}